import argparse
import agate
import agateexcel
from .sense_api import sense_api as api

'''
app: sense_importer==0.1
python version: 3.7
libraries: sense_api==0.1, agate-excel==0.2.2
create at: 2018-08-06
developer: Allan Carlos do Rosario Souza
e-mail: allan.carloss@gmail.com
'''


def print_result(action, result):
    print('Executing %s:\n%s\n' % (action, result))


class ReaderObjectsFromXLSX:
    def __init__(self, xlsx_path):
        self.xlsx_path = xlsx_path

    def get_objects_table(self, sheet):
        return agate.Table.from_xlsx(self.xlsx_path, sheet=sheet, skip_lines=2, header=True)


class Dimension:
    def __init__(self, _id, _label, _field, _title, _overwrite, expr_label):
        self.id = _id
        self.label = _label
        self.field = _field
        self.title = _title
        self.overwrite = _overwrite.upper() == 'SIM'
        self.expr_label = expr_label

    def __str__(self):
        return ', '.join((self.id, self.label, self.field, self.title, self.overwrite, self.expr_label))


class Measure:
    def __init__(self, _id, _label, _definition, _title, _overwrite, expr_label):
        self.id = _id
        self.label = _label
        self.definition = _definition
        self.title = _title
        self.overwrite = _overwrite.upper() == 'SIM'
        self.expr_label = expr_label

    def __str__(self):
        return ', '.join((self.id, self.label, self.definition, self.title, self.overwrite, self.expr_label))


class SenseImporter:
    def __init__(self, sense_api, dimensions, measures):
        self.sense_api = sense_api
        self.dimensions = dimensions
        self.measures = measures

    @staticmethod
    def destroy_objects(objects, existing_objects_ids, destroy_method, destroy_message):
        result = []
        for obj in objects:
            if obj.id in existing_objects_ids:
                result.append(destroy_method(obj.id))
        print_result(destroy_message, result)

    @staticmethod
    def existing_objects_ids(list_method, object_type, ):
        dic_objects = list_method()['result']['qLayout'][object_type]['qItems']
        objects_ids = []
        for dic_object in dic_objects:
            objects_ids.append(dic_object['qInfo']['qId'])

        return objects_ids

    def existing_dimensions_ids(self):
        return SenseImporter.existing_objects_ids(self.sense_api.list_dimensions, 'qDimensionList')

    def existing_measures_ids(self):
        return SenseImporter.existing_objects_ids(self.sense_api.list_measures, 'qMeasureList')

    def create_dimensions(self):
        result = []
        ids = self.existing_dimensions_ids()
        for dimension in self.dimensions:
            if dimension.id not in ids:
                result.append(
                    self.sense_api.create_dimension(
                        dimension.id, dimension.field, dimension.label, dimension.title, dimension.expr_label)
                )
        print(result)
        print_result('create_dimensions', result)

    def create_measures(self):
        result = []
        ids = self.existing_measures_ids()
        for measure in self.measures:
            if measure.id not in ids:
                result.append(
                    self.sense_api.create_measure(
                        measure.id, measure.label, measure.definition, measure.title, measure.expr_label)
                )
        print(result)
        print_result('create_measures', result)

    def execute(self):
        print_result('open_doc', self.sense_api.open_doc())

        print_result('get_active_doc', self.sense_api.get_active_doc())

        dimensions_to_destroy = [dimension for dimension in self.dimensions if dimension.overwrite]
        self.destroy_objects(
            dimensions_to_destroy, self.existing_dimensions_ids(), self.sense_api.delete_dimension, 'delete_dimensions')

        measures_to_destroy = [measure for measure in self.measures if measure.overwrite]
        self.destroy_objects(
            measures_to_destroy, self.existing_measures_ids(), self.sense_api.delete_measure, 'delete_measures')

        self.create_dimensions()
        self.create_measures()


class SenseImporterApp:
    @staticmethod
    def table_to_list(table, object_class, fields):
        dimensions = []
        for row in table:
            dimensions.append(
                object_class(
                    row[fields[0]], row[fields[1]], row[fields[2]], row[fields[3]], row[fields[4]], row[fields[5]]))

        return dimensions

    @staticmethod
    def table_dimension_to_list(table):
        return SenseImporterApp.table_to_list(
            table, Dimension, ['Id', 'Rótulo', 'Campo', 'Título', 'Sobrescrever', 'Exp Rótulo'])

    @staticmethod
    def table_measure_to_list(table):
        return SenseImporterApp.table_to_list(
            table, Measure, ['Id', 'Rótulo', 'Fórmula', 'Título', 'Sobrescrever', 'Exp Rótulo'])

    @staticmethod
    def execute(sense_host, sense_port_host, qvf_path, xlsx_path):
        try:
            sense_api = api.SenseAPI(sense_host, sense_port_host, qvf_path)

            reader = ReaderObjectsFromXLSX(xlsx_path)
            table_dimension = reader.get_objects_table('dimensões')
            dimensions = SenseImporterApp.table_dimension_to_list(table_dimension)
            table_measure = reader.get_objects_table('medidas')
            measures = SenseImporterApp.table_measure_to_list(table_measure)

            sense_importer = SenseImporter(sense_api, dimensions, measures)
            sense_importer.execute()
        finally:
            sense_api.close_doc()


if __name__ == '__main__':
    # class Parameters:
    #     def __init__(self):
    #         self.COMPUTADOR = 'localhost'
    #         self.PORTA = '4848'
    #         self.PAINEL = 'C:\\Users\\allan\\Documents\\Qlik\\Sense\\Apps\\Orlet Carros.qvf',
    #         self.PLANILHA = 'C:\\Users\\allan\\Documents\\Qlik\\Sense\\Apps\\Orlet Carros.xlsx'
    #
    # p = Parameters()
    # SenseImporterApp.execute(p.COMPUTADOR, str(p.PORTA), p.PAINEL, p.PLANILHA)

    parser = argparse.ArgumentParser()
    parser.add_argument(
        'COMPUTADOR', help='Nome ou IP do computador que possui o Qlik Sense Desktop aberto. Ex.: localhost')
    parser.add_argument(
        'PORTA', help='Porta utilizada pelo Qlik Sense Desktop.\nEx.: 4848', type=int)
    parser.add_argument(
        'PAINEL', help='Caminho onde se encontra o painel.\nEx.: C:\\Qlik\\Painel.qvf')
    parser.add_argument(
        'PLANILHA', help='Caminho onde se encontra a planilha com dimensões e medidas.\nEx.: C:\\Qlik\\Painel.xlsx')
    args = parser.parse_args()

    SenseImporterApp.execute(args.COMPUTADOR, str(args.PORTA), args.PAINEL, args.PLANILHA)
