import json
from urllib.parse import urlencode, quote
from websocket import create_connection


'''
app: sense_api==0.1
python version: 3.7
libraries: websocket-client==0.48.0 
create at: 2018-08-06
developer: Allan Carlos do Rosario Souza
e-mail: allan.carloss@gmail.com
'''


class SenseAPI:
    def __init__(self, sense_host, sense_port_host, qvf_path):
        self._sense_host = sense_host
        self._sense_port_host = sense_port_host
        self._qvf_path = qvf_path
        self._ws = None

    def open_doc(self):
        encoded_qvf_path = urlencode({'q': self._qvf_path}, encoding='utf-8', quote_via=quote).split('=')[1]
        url = 'ws://%s:%s/app/%s' % (self._sense_host, self._sense_port_host, encoded_qvf_path)
        self._ws = create_connection(url)

        return json.loads(self._ws.recv())

    def close_doc(self):
        return self._ws.close()

    def get_active_doc(self):
        get_active_doc = '''
        {
            "handle": -1,
            "method": "GetActiveDoc",
            "params": {}
        }
        '''
        self._ws.send(get_active_doc)

        return json.loads(self._ws.recv())

    def create_dimension(self, q_id, q_field_defs, q_field_labels, title, q_label_expression):
        dic_info = {"qId": q_id, "qType": "dimension"}
        dic_dim = {"qGrouping": 0, "qFieldDefs": [q_field_defs, ], "qFieldLabels": [q_field_labels, ], "qLabelExpression": q_label_expression}
        dic_meta_def = {"title": title}

        dic_prop = {"qInfo": dic_info, "qDim": dic_dim, "qMetaDef": dic_meta_def}
        dic_params = {"qProp": dic_prop}

        dic_create_dimension = {"jsonrpc": "2.0", "id": 2,
                                "method": "CreateDimension", "handle": 1, "params": dic_params}

        json_create_dimension = json.dumps(dic_create_dimension)
        self._ws.send(json_create_dimension)

        return json.loads(self._ws.recv())

    def list_dimensions(self):
        get_active_doc_handle = self.get_active_doc()['result']['qReturn']['qHandle']
        json_create_session_object = '''
        {
            "name": "DIMENSIONLIST",
            "method": "CreateSessionObject",
        	"handle": ''' + str(get_active_doc_handle) + ''',
            "params": [
                {
                    "qInfo": {
                    "qType": "DimensionList"
                },
                "qDimensionListDef": {
                    "qType": "dimension",
                    "qData": {
                        "title": "/title",
                        "tags": "/tags",
                        "grouping": "/qDim/qGrouping",
                        "info": "/qDimInfos"
                        }
                    }
                }
            ]
        }
        '''

        self._ws.send(json_create_session_object)
        response = self._ws.recv()
        dic_response = json.loads(response)
        handle = dic_response['result']['qReturn']['qHandle']

        json_get_layout = '{"method": "GetLayout", "handle": %s, "params": [], "outKey": -1, "id": 7}' % handle

        self._ws.send(json_get_layout)

        return json.loads(self._ws.recv())

    def delete_dimension(self, q_id):
        dic_destroy_dimension = {"handle": 1, "method": "DestroyDimension", "params": {"qId": q_id}}
        json_destroy_dimension = json.dumps(dic_destroy_dimension)

        self._ws.send(json_destroy_dimension)

        return json.loads(self._ws.recv())

    def create_measure(self, q_id, q_label, q_def, title, q_label_expression):
        dic_info = {"qId": q_id, "qType": "measure"}
        dic_measure = {
            "qLabel": q_label,
            "qDef": q_def,
            "qGrouping": 0,
            "qExpressions": ["", ],
            "qActiveExpression": 0,
            "qLabelExpression": q_label_expression
        }
        dic_meta_def = {"title": title}

        dic_prop = {"qInfo": dic_info, "qMeasure": dic_measure, "qMetaDef": dic_meta_def}
        dic_params = {"qProp": dic_prop}

        dic_create_measure = {"jsonrpc": "2.0", "id": 2, "method": "CreateMeasure", "handle": 1, "params": dic_params}

        json_create_measure = json.dumps(dic_create_measure)
        self._ws.send(json_create_measure)

        return json.loads(self._ws.recv())

    def list_measures(self):
        json_create_session_object = '''
        {  
           "method": "CreateSessionObject",  
           "handle": 1,  
           "params": [  
           {  
              "qInfo": {  
                 "qType": "MeasureList"  
              },  
              "qMeasureListDef": {  
                 "qType": "measure",  
                 "qData": {  
                    "title": "/title",  
                    "tags": "/tags"  
                 }  
              }  
           }
           ],  
           "outKey": -1,  
           "id": 3  
        } 
        '''

        self._ws.send(json_create_session_object)
        response = self._ws.recv()
        dic_response = json.loads(response)
        handle = dic_response['result']['qReturn']['qHandle']

        json_get_layout = '{"method": "GetLayout", "handle": %s, "params": [], "outKey": -1, "id": 7}' % handle
        self._ws.send(json_get_layout)

        return json.loads(self._ws.recv())

    def delete_measure(self, q_id):
        dic_destroy_measure = {"handle": 1, "method": "DestroyMeasure", "params": {"qId": q_id}}
        json_destroy_measure = json.dumps(dic_destroy_measure)

        self._ws.send(json_destroy_measure)

        return json.loads(self._ws.recv())







# sense_host = 'localhost'
# qvf_path = 'C:\\Users\\allan\\Documents\\Qlik\\Sense\\Apps\\Orlet Carros.qvf'

# encoded_qvf_path = urlencode({'q': qvf_path}, encoding='utf-8', quote_via=quote).split('=')[1]
# url = 'ws://%s:4848/app/%s' % (sense_host, encoded_qvf_path)
# ws = create_connection(url)
# print(ws.recv())
# print('')

# Active Doc
# ----------------

# get_active_doc = '''
# {
# 	"handle": -1,
# 	"method": "GetActiveDoc",
# 	"params": {}
# }
# '''

# ws.send(get_active_doc)
# result_get_active_doc = ws.recv()
# print(result_get_active_doc)
# print('')

# get_active_doc_handle = json.loads(result_get_active_doc)['result']['qReturn']['qHandle']
# print(get_active_doc_handle)
# print('')

# Create Dimension
# ----------------

# dic_info = {"qId": "dim_cddepartamento", "qType": "dimension"}
# dic_dim = {"qGrouping": 0, "qFieldDefs": ["EFE_cdDepartamento",], "qFieldLabels": ["Código do departamento",], "qLabelExpression": ""}
# dic_meta_def = {"title": "Departamento"}

# dic_prop = {"qInfo": dic_info, "qDim": dic_dim, "qMetaDef": dic_meta_def}

# dic_params = {"qProp": dic_prop}

# dic_create_dimension = {"jsonrpc": "2.0", "id": 2, "method": "CreateDimension", "handle": 1, "params": dic_params}

# json_create_dimension = json.dumps(dic_create_dimension)
# print(json_create_dimension)
# print('')

# ws.send(json_create_dimension)
# print(ws.recv())
# print('')

# Create Measure
# --------------

# dic_info = {"qId": "mea_vlvalor", "qType": "measure"}
# dic_measure = {"qLabel": "Valor", "qDef": "=Sum(EFE_vlValor)", "qGrouping": 0, "qExpressions": ["",], "qActiveExpression": 0, "qLabelExpression": ""}
# dic_meta_def = {"title": "Valor"}

# dic_prop = {"qInfo": dic_info, "qMeasure": dic_measure, "qMetaDef": dic_meta_def}

# dic_params = {"qProp": dic_prop}

# dic_create_measure = {"jsonrpc": "2.0", "id": 2, "method": "CreateMeasure", "handle": 1, "params": dic_params}

# json_create_measure = json.dumps(dic_create_measure)
# print(json_create_measure)
# print('')

# ws.send(json_create_measure)
# print(ws.recv())
# print('')

# Destroy Dimension
# -----------------

# dic_destroy_dimension = {"handle": 1, "method": "DestroyDimension", "params": {"qId": "dim_cddepartamento"}}
# json_destroy_dimension = json.dumps(dic_destroy_dimension)
# print(json_destroy_dimension)
# print('')

# ws.send(json_destroy_dimension)
# print(ws.recv())
# print('')

# List Measures
# ---------------

# json_create_session_object = '''
# {  
#    "method": "CreateSessionObject",  
#    "handle": 1,  
#    "params": [  
#    {  
#       "qInfo": {  
#          "qType": "MeasureList"  
#       },  
#       "qMeasureListDef": {  
#          "qType": "measure",  
#          "qData": {  
#             "title": "/title",  
#             "tags": "/tags"  
#          }  
#       }  
#    }
#    ],  
#    "outKey": -1,  
#    "id": 3  
# } 
# '''

# ws.send(json_create_session_object)
# response = ws.recv()
# print(response)
# print('')

# dic_response = json.loads(response)
# print(dic_response)

# handle = dic_response['result']['qReturn']['qHandle']

# json_get_layout = '{"method": "GetLayout", "handle": %s, "params": [], "outKey": -1, "id": 7}' % handle

# ws.send(json_get_layout)
# print(ws.recv())
# print('')

# List Dimensions
# ---------------

# json_create_session_object = '''
# {
# 		"name": "DIMENSIONLIST",
# 		"method": "CreateSessionObject",
# 		"handle": ''' + str(get_active_doc_handle) + ''',
# 		"params": [
# 			{
# 				"qInfo": {
# 					"qType": "DimensionList"
# 				},
# 				"qDimensionListDef": {
# 					"qType": "dimension",
# 					"qData": {
# 						"title": "/title",
# 						"tags": "/tags",
# 						"grouping": "/qDim/qGrouping",
# 						"info": "/qDimInfos"
# 					}
# 				}
# 			}
# 		]
# 	}
# '''

# ws.send(json_create_session_object)
# response = ws.recv()
# print(response)
# print('')

# dic_response = json.loads(response)
# print(dic_response)
# print('')


# handle = dic_response['result']['qReturn']['qHandle']

# json_get_layout = '{"method": "GetLayout", "handle": %s, "params": [], "outKey": -1, "id": 7}' % handle

# ws.send(json_get_layout)
# print(ws.recv())
# print('')

# ws.close()
