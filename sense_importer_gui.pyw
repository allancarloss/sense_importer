# coding: utf-8
import traceback
from tkinter import *
from tkinter import messagebox
from sense_importer.sense_importer import SenseImporterApp


VERSION = '0.1.2'
DEVELOPER = 'Allan Carlos'
EMAIL = 'allan.carloss@gmail.com'

window_title_text = 'Sense Importer versão: %s' % VERSION

main_window = Tk()
main_window.title(window_title_text)
main_window.geometry('500x200+200+200')
main_window.resizable(False, False)

Label(main_window, text='Abra o Qlik Sense Desktop antes de importar.', fg='red').place(x=10, y=0)

Label(main_window, text='Computador:').place(x=10, y=25)

tx_computador = StringVar()
ed_computador = Entry(main_window, width=64, textvariable=tx_computador)
ed_computador.place(x=100, y=25)
tx_computador.set('localhost')

Label(main_window, text='Porta:').place(x=10, y=50)

tx_porta = StringVar()
ed_porta = Entry(main_window, width=64, textvariable=tx_porta)
ed_porta.place(x=100, y=50)
tx_porta.set('4848')

Label(main_window, text='Painel:').place(x=10, y=75)

tx_painel = StringVar()
ed_painel = Entry(main_window, width=64, textvariable=tx_painel)
ed_painel.place(x=100, y=75)
tx_painel.set('C:\\Users\\allan\\Documents\\Qlik\\Sense\\Apps\\Orlet Carros.qvf')

Label(main_window, text='Xlsx:').place(x=10, y=100)

tx_xlsx = StringVar()
ed_xlsx = Entry(main_window, width=64, textvariable=tx_xlsx)
ed_xlsx.place(x=100, y=100)
tx_xlsx.set('C:\\Users\\allan\\Documents\\Qlik\\Sense\\Apps\\Orlet Carros.xlsx')


def bt_import_click():
    try:
        SenseImporterApp.execute(ed_computador.get(), ed_porta.get(), ed_painel.get(), ed_xlsx.get())
        messagebox.showinfo(window_title_text, 'Importação com concluída com sucesso.')
    except Exception as e:
        print(traceback.format_exception(None,  # <- type(e) by docs, but ignored
                                         e, e.__traceback__),
              file=sys.stderr, flush=True)
        messagebox.showerror(window_title_text, 'Falha na importação. Verifique os dados informados.')


Button(main_window, text='Importar Dimensões and Medidas', command=bt_import_click).place(x=10, y=135)

Label(main_window, text='Desenvolvido por: %s (e-mail: %s)' % (DEVELOPER, EMAIL), fg='blue').place(x=10, y=175)

main_window.mainloop()
